import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchSurveys } from './components/FetchSurveys';
import { FetchSubmissions } from './components/FetchSubmissions';
import { NewSurvey } from './components/NewSurvey';
import { NewSubmission } from './components/NewSubmission';

// Create routes for the pages available and pass to layout component and return as App. 
export default class App extends Component {
  displayName = App.name

  render() {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/fetchsurveys' component={FetchSurveys} />
        <Route path='/fetchsubmissions' component={FetchSubmissions} />
        <Route path='/newsurvey' component={NewSurvey} />
        <Route path='/newsubmission' component={NewSubmission} />
      </Layout>
    );
  }
}
