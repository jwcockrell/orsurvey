import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// Test app can be created
it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
