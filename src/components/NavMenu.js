﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Glyphicon, Nav, Navbar, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import './NavMenu.css';

// Navigation menu component
export class NavMenu extends Component {
  displayName = NavMenu.name

  render() {
    return (
      <Navbar inverse fixedTop fluid collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to={'/'}>react2</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <LinkContainer to={'/'} exact>
              <NavItem>
                <Glyphicon glyph='home' /> Home
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/fetchsurveys'}>
              <NavItem>
                <Glyphicon glyph='th-list' /> Surveys
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/newsurvey'}>
              <NavItem>
                <Glyphicon glyph='th-list' /> New Survey
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/fetchsubmissions'}>
              <NavItem>
                <Glyphicon glyph='th-list' /> Submissions
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/newsubmission'}>
              <NavItem>
                <Glyphicon glyph='th-list' /> New Submission
              </NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}
