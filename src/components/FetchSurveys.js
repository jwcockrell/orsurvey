import React, { Component } from 'react';

// Show list of surveys
export class FetchSurveys extends Component {
  displayName = FetchSurveys.name

  // fetch survey list from Web API
  constructor(props) {
    super(props);
    this.state = { surveys: [], loading: true };

//    fetch('http://localhost:5000/api/survey')
    fetch('http://survey20180605024628.azurewebsites.net/api/survey')
      .then(response => response.json())
      .then(data => {
        this.setState({ surveys: data, loading: false });
      });
  }

  // Render table of results 
  static renderSurveysTable(surveys) {
    return (
      <table className='table'>
        <thead>
          <tr>
            <th>Name</th>
            <th>Questions</th>
          </tr>
        </thead>
        <tbody>
          {surveys.map(survey =>
            <tr key={survey.id}>
              <td>{survey.desc}</td>
              <td>
              <ul>
              {survey.questions.map(question =>
              <li key={question.id}>{question.name}</li>
              )}
              </ul></td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  // Render survey table
  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
      : FetchSurveys.renderSurveysTable(this.state.surveys);

    return (
      <div>
        <h1>Surveys</h1>
        <p>Surveys created.</p>
        {contents}
      </div>
    );
  }
}
