import React, { Component } from 'react';

// Create and save new question submissions
export class NewSubmission extends Component {
  displayName = NewSubmission.name
  
  constructor(props){
    super(props);
    this.saveSubmission = this.saveSubmission.bind(this);
  }

  saveSubmission(){
    var data = JSON.parse(JSON.stringify(this.submissionText.value));
    console.log(data);

    //fetch('http://localhost:5000/api/submission', {
    fetch('http://survey20180605024628.azurewebsites.net/api/submission', {
      method:"POST",
      body: data,
      mode: 'cors',
      headers : {
          'Accept'     : 'application/json',
          'Content-Type':'application/json'}
      }
    );
  }
  
  sampledata = '{\n    "Name" : "John",\n    "Answers" : [\n      {\n        "QuestionID" : 3,\n        "Name" : "Eggs Benedict Royale"\n      }, {\n        "QuestionID" : 4,\n        "Name" : "Corn Flakes with skimmed milk"\n      }]\n  }';
  render() {
    return (
      <div>
        <h1>New Submission</h1>
        <p>Adjust below sample to create a new submission</p>
        <textarea ref={(input) => { this.submissionText = input; }} cols="120" rows="10">
        { this.sampledata } 
        </textarea>
        <br />
        <button onClick={this.saveSubmission} >Save Submission</button>
      </div>
    );
  }
}
