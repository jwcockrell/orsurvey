import React, { Component } from 'react';

// Create and save new surveys
export class NewSurvey extends Component {
  displayName = NewSurvey.name
  
  constructor(props){
    super(props);
    this.saveSurvey = this.saveSurvey.bind(this);
  }

  saveSurvey(){
    var data = JSON.parse(JSON.stringify(this.surveyText.value));
    console.log(data);

    //fetch('http://localhost:5000/api/survey', {
    fetch('http://survey20180605024628.azurewebsites.net/api/survey', {
      method:"POST",
      body: data,
      mode: 'cors',
      headers : {
          'Accept'     : 'application/json',
          'Content-Type':'application/json'}
      }
    );
  }
  
  sampledata = '{	\n  "Desc": "Breakfast Survey",\n  "Questions" : [\n    {"Name" : "What is your Favourite"}, \n    {"Name" : "What is your least Favourite"}\n  ]\n}';
  render() {
    return (
      <div>
        <h1>New Survey</h1>
        <p>Adjust below sample to create a new survey</p>
        <textarea ref={(input) => { this.surveyText = input; }} cols="120" rows="10">
        { this.sampledata } 
        </textarea>
        <br />
        <button onClick={this.saveSurvey} >Save Survey</button>
      </div>
    );
  }
}
