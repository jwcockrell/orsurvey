// Component for showing submissions.
import React, { Component } from 'react';

export class FetchSubmissions extends Component {
  displayName = FetchSubmissions.name

  // on construction, fetch data and add to state
  constructor(props) {
    super(props);
    this.state = { submissions: [], loading: true };

    //fetch('http://localhost:5000/api/submission',{
    fetch('http://survey20180605024628.azurewebsites.net/api/submission',{
      mode: 'cors'})
      .then(response => response.json())
      .then(data => {
        this.setState({ submissions: data, loading: false });
      });
  }

  // Create table from API results
  static renderSubmissionsTable(submissions) {
    return (
      <table className='table'>
        <thead>
          <tr>
            <th>Name</th>
            <th>Answers</th>
          </tr>
        </thead>
        <tbody>
          {submissions.map(submission =>
            <tr key={submission.id}>
              <td>{submission.name}</td>
              <td><ul>
              {submission.answers.map(answer =>
              <li key={answer.id}>{answer.name}</li>
              )}
              </ul></td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  // Render table of results
  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
      : FetchSubmissions.renderSubmissionsTable(this.state.submissions);

    return (
      <div>
        <h1>Submissions</h1>
        <p>Submitted surveys.</p>
        {contents}
      </div>
    );
  }
}
