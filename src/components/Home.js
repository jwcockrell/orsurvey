import React, { Component } from 'react';

// Home component, show welcome message
export class Home extends Component {
  displayName = Home.name

  render() {
    return (
      <div>
        <h1>Survey application</h1>
        <h3>John Cockrell. </h3>
        <p>Use the left hand menu to browse.</p>
      </div>
    );
  }
}
