// A test test file to check testing is working, use as a model for other tests.

// scenario, saving a good survey
test('Given I am an admin and have entered good survey data, when I submit it, then expect it to save', () => {
    expect(false).toBe(true);
})

// scenario, saving a bad survey
test('Given I am an admin and have entered no survey data, when I submit it, then expect it to fail to save and give an error', () => {
    expect(false).toBe(true);
})

// scenario, saving a good submission
test('Given I am a user and have entered good survey submission data, when I submit it, then expect it to save', () => {
    expect(false).toBe(true);
})

// scenario, saving a bad submission
test('Given I am an admin and have entered no submission data, when I submit it, then expect it to fail to save and give an error', () => {
    expect(false).toBe(true);
})

// scenario, viewing survey list
test('Given I am an admin and click the survey page, then expect it to show a list of surveys', () => {
    expect(false).toBe(true);
})

// scenario, viewing submission list
test('Given I am an admin and click the submission page, then expect it to show a list of submissions and answers', () => {
    expect(false).toBe(true);
})

// scenario, going to home page
test('Given I am a user, when I click home, then expect the home page to load with introduction', () => {
    expect(false).toBe(true);
})

// scenario, saving a  to submissions page
test('Given I am a user, when I click submissions, then expect a list of submissions to load', () => {
    expect(false).toBe(true);
})


// Test Submission returns correct results
// Test Save can save
